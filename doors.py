import random
def setUpGame():
	number = random.randint(0, 2)
	blankDoors = ['', '', '']
	for i in range (len(blankDoors)):
		if i == number:
			blankDoors[i] = 'Car'
		else:
			blankDoors[i] = 'Goat'
	return blankDoors

# Ведущий открывает дверь
def reveal(currentGame):
	number = random.randint(1, 2)
	if currentGame[number] == 'Car':
		if number == 1:
			number = 2;
		currentGame[number] = 'Reveal'
	else:
		currentGame[number] = 'Reveal'
	return currentGame




def swap(currentGame):
	if currentGame[1] == 'Reveal':
		currentGame[0], currentGame[2] = currentGame[2], currentGame[0]
	else:
		currentGame[0], currentGame[1] = currentGame[1], currentGame[0]
	return currentGame


totalGames = 1e4
i = 0
wins = 0
losses = 0

while i < totalGames:
	currentGame = setUpGame()
	currentGame = reveal(currentGame)

	# количество побед, если изменить решение
	# currentGame = swap(currentGame);

	if currentGame[0] == 'Car':
		wins += 1
	else:
		losses += 1
	i += 1
nwins = wins/(wins + losses)*100
print(nwins)




